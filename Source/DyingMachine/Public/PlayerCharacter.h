// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Character.h"
#include "PlayerCharacter.generated.h"

class AProjectile;

UCLASS(ClassGroup = (Custom), meta = (BlueprintSpawnableComponent))
class DYINGMACHINE_API APlayerCharacter : public ACharacter
{
	GENERATED_BODY()

public:
	// Sets default values for this character's properties
	APlayerCharacter();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	// Called to bind functionality to input
	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;

	UFUNCTION(BlueprintCallable, Category = "Firing")
	void Fire();

	UFUNCTION(BlueprintCallable, Category = "Death")
	void CameraDeath();

	UFUNCTION(BlueprintCallable, Category = "Victory")
	void Victory();

	UFUNCTION(BlueprintCallable, Category = "Power")
	void Recharge(float RechargeAmount);

	UFUNCTION(BlueprintCallable, Category = "Movement")
	void OnJump();

	UPROPERTY(BlueprintReadOnly, Category = "Movement")
	bool DisableMovement = false;

	UPROPERTY(BlueprintReadOnly, Category = "Power")
	float CurrentPower = 0;

private:
	void OnRechargeFinished();

	float CurrentDecayRate = 0;
	float InitialDecayRate;
	float CurrentRechargeRate;
	bool IsRecharging = false;

	UPROPERTY(EditDefaultsOnly, Category = "Setup")
	TSubclassOf<AProjectile> ProjectileBlueprint;

	UPROPERTY(EditDefaultsOnly, Category = "Power")
	float InitialPower = 100;

	UPROPERTY(EditDefaultsOnly, Category = "Power")
	float BaseDecayRate = 1;

	UPROPERTY(EditDefaultsOnly, Category = "Power")
	float JumpCost = 5;

	UPROPERTY(EditDefaultsOnly, Category = "Power")
	float ShootCost = 50;
};
