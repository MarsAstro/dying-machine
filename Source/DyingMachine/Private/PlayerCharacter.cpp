// Fill out your copyright notice in the Description page of Project Settings.

#include "PlayerCharacter.h"
#include "Projectile.h"
#include "Engine/World.h"
#include "GameFramework/CharacterMovementComponent.h"
#include "Runtime/Engine/Public/TimerManager.h"

// Sets default values
APlayerCharacter::APlayerCharacter()
{
 	// Set this character to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
}

// Called when the game starts or when spawned
void APlayerCharacter::BeginPlay()
{
	Super::BeginPlay();
	
	CurrentDecayRate = BaseDecayRate;
	InitialDecayRate = BaseDecayRate;
	CurrentPower = InitialPower;
	CurrentRechargeRate = 0;
}

// Called every frame
void APlayerCharacter::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	if (CurrentPower > 0.0f)
	{
		if (IsRecharging)
			CurrentDecayRate = CurrentRechargeRate;
		else if (GetVelocity().Size())
			CurrentDecayRate = BaseDecayRate * 2;
		else
			CurrentDecayRate = BaseDecayRate;

		CurrentPower -= CurrentDecayRate * DeltaTime;
	}
	else
	{
		CurrentPower = 0.0f;
		DisableMovement = true;
	}
	
}

// Called to bind functionality to input
void APlayerCharacter::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);
}

void APlayerCharacter::OnJump()
{
	if (!GetVelocity().Z)
		CurrentPower -= JumpCost;
}

void APlayerCharacter::Fire()
{
	if (!ensure(ProjectileBlueprint))
		return;

	// The projectile should appear to the right of the player, not in the middle of it
	// as that is where the "arm" would be
	FVector SpawnLocation = GetActorLocation() 
							+ (GetActorRightVector() * 40) 
							+ (GetActorForwardVector() * 10)
							+ (GetActorUpVector() * 30);

	AProjectile* SpawnedProjectile = GetWorld()->SpawnActor<AProjectile>
	(
		ProjectileBlueprint,
		SpawnLocation,
		GetControlRotation()
	);

	SpawnedProjectile->LaunchProjectile();
	CurrentPower -= ShootCost;
}

void APlayerCharacter::CameraDeath()
{
	DisableMovement = true;
	BaseDecayRate = CurrentPower;
}

void APlayerCharacter::Victory()
{
	BaseDecayRate = -1000.0f;
}

void APlayerCharacter::Recharge(float RechargeAmount)
{
	IsRecharging = true;
	CurrentRechargeRate = -RechargeAmount;

	FTimerHandle Timer;
	GetWorld()->GetTimerManager().SetTimer(Timer, this, &APlayerCharacter::OnRechargeFinished, 1.0f, false);
}

void APlayerCharacter::OnRechargeFinished()
{
	IsRecharging = false;
}